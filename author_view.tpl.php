<?php
// $Id$
/**
* @file 
* This template file is responsible for the view of the author information in the nodes
*
* Available variables : 
*  - $author_info : is information about the auther of the node... the proberties is the field name of table nodeauthor in database
*  - $node_details : the details of the current node
*  - $path : Current module path
*  - $site_path : current drupal site full url
*  - $add_css : this param add the css file to this template
*  - $add_js : this param add the js file to this template
*  - $module_path : the path of the module
*/

?>

 

<div  class="author-block"> <!-- and author-block in css -->
  <?php
		if (!empty($node_details->picture)) {   // print the user profile pic if exist 
		  echo "<img src='".$site_path.$node_details->picture."'alt='".check_plain($author_info->nodeauthor_name)."'  id='author-pic' />" ;
		}
		
		else {  // if the user havn't profile picture the module will present the default picture
		  $no_picture_path = $site_path.$module_path.'/images/no-pic.jpg' ; 
		  echo "<img src='".$no_picture_path."'alt='".check_plain($author_info->nodeauthor_name)."'  id='author-pic' />" ; 
		}
  ?>
	<div id="author-content">
		<div id="author-content-margin">
				<?php $user_path = $site_path."user/".$node_details->uid ;   ?>  <!-- get the user profile url -->
				<?php if(!empty($node_details->uid)): ?> <!-- the user exist -->
				<?php if(!empty($author_info->nodeauthor_name)): ?> <label> Name:</label> <a href="<?php print $user_path  ;  ?>" > <?php print check_plain($author_info->nodeauthor_name) ; ?> </a> <?php else: print "<label>Name : </label> no name" ; endif ; ?> <br /> <br /> <!-- print user name if exist -->
				<?php if(!empty($author_info->nodeauthor_mail)): ?> <label> E-mail:</label> <a href="<?php print 'mailto:'.$author_info->nodeauthor_mail  ;  ?>" > <?php print check_plain($author_info->nodeauthor_mail) ; ?> </a> <?php else: print "<label>E-mail : </label> no mail" ; endif ; ?> <br /> <br /> <!-- print user mail if exist -->
				<?php if(!empty($author_info->nodeauthor_signature)):?>  <?php print "<label>Signature : </label> ".check_plain($author_info->nodeauthor_signature) ; else: print "<label>Signature : </label> no signature" ; endif ;?> <!-- print user signature if exist -->			
				<?php endif; ?>
				
				
				<?php if(empty($node_details->uid)) :?>  <!-- the user deleted or anonymouse -->
				<label>Name : </label><?php print ($author_info->nodeauthor_name ? check_plain($author_info->nodeauthor_name) : 'Anonymous') ; ?>	<br/> <br/> <!-- print anonymouse name -->
				<label>E-mail : </label><?php print ($author_info->nodeauthor_mail ? '<a href="mailto:'.$author_info->nodeauthor_mail.'">'.check_plain($author_info->nodeauthor_mail)."</a>" : 'Not Exist') ; ?>	<br/> <br/> <!-- print anonymouse mail -->
				<label>Signature : </label><?php print ($author_info->nodeauthor_signature ? check_plain($author_info->nodeauthor_signature) : 'Not Exist') ; ?>	<br/> <br/> <!-- print anonymouse signature -->
				<?php endif ; ?>
				

		</div>
	</div>
	

</div>
