/*global Drupal $ document jQuery */
if (Drupal.jsEnabled) {
    $(document).ready(
        function () {
            $("a:contains(Author Info)").toggle(
                function () {
                    $('#collapsible-fieldset')
                        .animate(
                            {left : "0%"},
                            1500,
                            "swing"  
                        ).animate(
                            {width: "17%"}
                        ).animate(
                            {"border-radius" : "0px", "-webkit-border-radius" : "0px", "-moz-border-radius" : "0px"},
                            1000
                        ); 

                    $('#author-pic , #author-content').hide(); 
                }, 
                function () {
                    $('#collapsible-fieldset')
                        .animate(
                            {left : "25%"},
                            1500,
                            "swing"  
                        ).animate(
                            {width: "60%"},
                            1500
                        ).animate(
                            {"border-radius" : "12px", "-webkit-border-radius" : "12px", "-moz-border-radius" : "12px"},
                            1000,
                            'linear',
                            function () {
                                $('#author-pic, #author-content').fadeIn(1000); 
                            }
                        );
                }
            ); 

        }	

    ); 

}

